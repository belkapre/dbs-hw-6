package cvut.fel.dbs.lib;

/**
 * The only purpose of this class is because of problems with jar packaging, that this is supposed to solve.
 */
public class FakeMain {
    public static void main(String[] args) {
        Application.main(args);
    }
}
