package cvut.fel.dbs.lib.dao;

import cvut.fel.dbs.lib.model.Book;
import cvut.fel.dbs.lib.model.Order;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

public class BookDao {
    private EntityManager em;
    private final int PAGESIZE = 10;

    public BookDao(EntityManager em) {
        this.em = em;
    }

    public List<Book> findPage(int page){
        TypedQuery<Book> query = em.createQuery("SELECT b FROM Book b ORDER BY b.name ASC", Book.class);
        query.setMaxResults(PAGESIZE);
        query.setFirstResult(PAGESIZE * (page - 1));
        return query.getResultList();
    }

    public void create(Book p){
        if (this.find(p.getIsbn()) != null) {
            throw new IllegalArgumentException("Book with this ISBN already exists.");
        }
        em.persist(p);
    }

    public Book find(String isbn){
        return em.find(Book.class, isbn);
    }

    public Book merge(Book p){
        return em.merge(p);
    }

    public void delete(Book p){
        em.remove(p);
    }

    public int getPages(){
        Long items = (Long) em.createQuery("SELECT COUNT(b) FROM Book b ").getSingleResult();
        return (int) ((items / PAGESIZE) + 1);
    }
}
