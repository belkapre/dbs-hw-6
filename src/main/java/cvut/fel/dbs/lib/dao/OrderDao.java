package cvut.fel.dbs.lib.dao;

import cvut.fel.dbs.lib.model.Book;
import cvut.fel.dbs.lib.model.BookInOrder;
import cvut.fel.dbs.lib.model.Order;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class OrderDao {
    private EntityManager em;
    private final int PAGESIZE = 11;

    public OrderDao(EntityManager em) {
        this.em = em;
    }

    public List<Order> findAll(){
        return em.createQuery("SELECT p FROM Order p", Order.class).getResultList();
    }

    public List<Order> findPage(int page){
        TypedQuery<Order> query = em.createQuery("SELECT o FROM Order o ORDER BY o.id ASC", Order.class);
        query.setMaxResults(PAGESIZE);
        query.setFirstResult(PAGESIZE * (page - 1));
        return query.getResultList();
    }

    public void create(Order p){
        for (BookInOrder b : p.getBookInOrder()){
            em.persist(b);
        }
        em.persist(p);
    }

    public int getPages(){
        Long items = (Long) em.createQuery("SELECT COUNT(o) FROM Order o ").getSingleResult();
        return (int) ((items / PAGESIZE) + 1);
    }
}
