package cvut.fel.dbs.lib.dao;

import cvut.fel.dbs.lib.model.BookInOrder;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class BookInOrderDao {
    private EntityManager em;

    public BookInOrderDao(EntityManager em) {
        this.em = em;
    }

    public void create(BookInOrder p){
        em.persist(p);
    }

    public BookInOrder find(int id){
        return em.find(BookInOrder.class, id);
    }

    public BookInOrder merge(BookInOrder p){
        return em.merge(p);
    }

    public void delete(BookInOrder p){
        em.remove(p);
    }
}
