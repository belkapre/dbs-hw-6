package cvut.fel.dbs.lib.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "bookorder")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String deliveryTrackingNumber;

    @Column(name="orderplacedtimestamp")
    private Date orderPlacedTimestamp;

    private Integer price;

    @Column(name="customer_email")
    private String customer;

    @OneToMany(mappedBy="order")
    private List<BookInOrder> bookInOrder;

    public Integer getId() {
        return id;
    }

    public String getDeliveryTrackingNumber() {
        return deliveryTrackingNumber;
    }

    public Date getOrderPlacedTimestamp() {
        return orderPlacedTimestamp;
    }

    public Integer getPrice() {
        return price;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setDeliveryTrackingNumber(String deliveryTrackingNumber) {
        this.deliveryTrackingNumber = deliveryTrackingNumber;
    }

    public void setOrderPlacedTimestamp(Date orderPlacedTimestamp) {
        this.orderPlacedTimestamp = orderPlacedTimestamp;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public List<BookInOrder> getBookInOrder() {
        return bookInOrder;
    }

    public void setBookInOrder(List<BookInOrder> bookInOrder) {
        this.bookInOrder = bookInOrder;
    }

    @Override
    public String toString() {
        return "Order: " + id + " Price: " + price + "Kč " + "Placed by: " + customer;
    }
}
