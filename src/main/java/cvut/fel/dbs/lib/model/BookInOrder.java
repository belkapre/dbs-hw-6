package cvut.fel.dbs.lib.model;

import cvut.fel.dbs.lib.Application;
import cvut.fel.dbs.lib.dao.BookDao;

import javax.persistence.*;

/**
 * Entity mapping the manytomany relationship between Order and Book.
 * It needs to be a separate class, because it needs to hold the quantity value.
 */
@Entity
@Table(name="booksinorder")
public class BookInOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer quantity;

    @ManyToOne
    @JoinColumn(name = "isbn")
    private Book book;

    @ManyToOne
    @JoinColumn(name = "orderid")
    private Order order;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        if (quantity < 1 || quantity > Math.pow(quantity, 31)){
            throw new IllegalArgumentException("Invalid quantity");
        }
        this.quantity = quantity;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    /**
     * Finds a book with a given isbn and sets the book.
     * @param isbn primary key to Book
     */
    public void setBookByIsbn(String isbn){
        BookDao bookDao = new BookDao(Application.getEm());
        Book book = bookDao.find(isbn);
        if (book != null) this.book = book;

        else{
            throw new IllegalArgumentException("Book with this isbn not found");
        }
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "Quantity: " + quantity + " " + book;
    }
}
