package cvut.fel.dbs.lib.model;

import javax.persistence.*;

@Entity
public class Book {

    @Id
    private String isbn;

    private String name;

    private String author;

    private int edition;

    private int price;

    private int releaseyear;

    private String publisher;

    public int getEdition() {
        return edition;
    }

    public void setEdition(int edition) {
        this.edition = edition;
    }


    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        if ((isbn.length() != 13) && (isbn.length() != 10)){
            throw new IllegalArgumentException("Isbn must be 10 or 13 digits long");
        }

        if (!isbn.matches("\\d+")){
            throw new IllegalArgumentException("Isbn must contain only digits");
        }
        this.isbn = isbn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.length() == 0){
            throw new IllegalArgumentException("Empty name");
        }

        if (name.length() > 200){
            throw new IllegalArgumentException("Book name cannot be longer than 200 characters.");
        }

        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        if (author.length() == 0){
            throw new IllegalArgumentException("Empty author");
        }

        if (author.length() > 140){
            throw new IllegalArgumentException("Author name cannot be longer than 140 characters.");
        }
        this.author = author;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        if (price < 0){
            throw new IllegalArgumentException("Price cannot be negative.");
        }

        if (price > Math.pow(2, 31)){
            throw new IllegalArgumentException("Invalid price");
        }
        this.price = price;
    }

    public int getReleaseyear() {
        return releaseyear;
    }

    public void setReleaseyear(int releaseyear) {
        if (price < 0){
            throw new IllegalArgumentException("Release year cannot be negative.");
        }

        if (releaseyear > 2500) {
            throw new IllegalArgumentException("Invalid release year");
        }
        this.releaseyear = releaseyear;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        if (publisher.length() == 0) {
            throw new IllegalArgumentException("Publisher empty");
        }

        if (publisher.length() > 100){
            throw new IllegalArgumentException("Publisher can be at most 100 characters long");
        }

        this.publisher = publisher;
    }

    @Override
    public String toString() {
        return name + " ISBN:" + isbn;
    }
}
