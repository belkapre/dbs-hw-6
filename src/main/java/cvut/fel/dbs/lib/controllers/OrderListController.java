package cvut.fel.dbs.lib.controllers;

import cvut.fel.dbs.lib.Application;
import cvut.fel.dbs.lib.dao.BookInOrderDao;
import cvut.fel.dbs.lib.dao.OrderDao;
import cvut.fel.dbs.lib.model.BookInOrder;
import cvut.fel.dbs.lib.model.Order;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class OrderListController extends AbstractPaginatedController implements Initializable {
    @FXML
    private ListView listView;

    private static Order selectedOrder;

    private static BookInOrder selectedBookInOrder;

    private List<Order> currentPage;

    private static EntityManager em;

    @FXML
    private Button backToOrders;

    @FXML
    private Button addBook;

    @FXML
    private Button deleteBook;

    /**
     * Invoked after loading the controler with FXMLloader.
     * Sets up necessary atributtes, listeners and shows the first page.
     *
     * @param location supplied automatically by JAVAfx
     * @param resources supplied automatically by JAVAfx
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.em = Application.getEm();
        setLastPage();
        showPage();
        setListenerForOrders();
        previousPage.setDisable(true);
        disableOrderDetailButtons();
    }

    /**
     * Sets the total amount of pages (or the number of the last page)
     */
    @Override
    protected void setLastPage(){
        OrderDao orderDao = new OrderDao(em);
        lastPage = orderDao.getPages();
    }

    /**
     * Enables buttons relating to a specified order when choosing one from the list.
     */
    private void enableOrderDetailButtons(){
        backToOrders.setDisable(false);
        addBook.setDisable(false);
        deleteBook.setDisable(false);
    }

    /**
     * Disables all buttons relating to order details when browsing the list.
     */
    private void disableOrderDetailButtons(){
        backToOrders.setDisable(true);
        addBook.setDisable(true);
        deleteBook.setDisable(true);
    }

    /**
     * Called after clicking on the back to orders button.
     * It sets up the buttons according to the current order page and shows the list with orders.
     */
    @FXML
    private void goBackToOrderList(){
        showPage();
        restorePageButtons();
        disableOrderDetailButtons();
        setListenerForOrders();
    }

    /**
     * Adds onclick listener to the order list, after clicking on an order its detail is shown.
     * (books the order contains)
     */
    private void setListenerForOrders(){
        listView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                showDetailOfOrder();
            }
        });
    }

    /**
     * Sets onlick listener on list of books in an order.
     * After clicking on a book, the book is selected.
     */
    private void setListenerForBooks(){
        listView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                selectedBookInOrder = (BookInOrder) listView.getSelectionModel().getSelectedItem();
            }
        });
    }

    /**
     * Loads a page of orders into memory and shows it in the list.
     */
    @Override
    protected void showPage(){
        OrderDao orderDao = new OrderDao(em);
        currentPage = orderDao.findPage(currentPageNumber);
        listView.getItems().clear();
        for (Order order : currentPage){
            listView.getItems().add(order);
        }
    }

    /**
     * Shows list of books in order, disables the arrow buttons for navigating pages of orders,
     * enables the buttons for handling orders.
     */
    private void showDetailOfOrder(){
        disablePageButtons();
        if (listView.getSelectionModel().getSelectedItem() == null && selectedOrder == null) return;
        else if (listView.getSelectionModel().getSelectedItem() instanceof Order){
            selectedOrder = (Order) listView.getSelectionModel().getSelectedItem();
        }
        enableOrderDetailButtons();

        listView.getItems().clear();

        for (BookInOrder book : selectedOrder.getBookInOrder()){
            listView.getItems().add(book);
        }

        setListenerForBooks();
    }

    /**
     * Disables buttons for navigating pages.
     */
    private void disablePageButtons(){
        nextPage.setDisable(true);
        previousPage.setDisable(true);
    }

    /**
     * Enables correct arrow buttons for navigating pages of orders.
     */
    private void restorePageButtons(){
        previousPage.setDisable(currentPageNumber == 1);
        nextPage.setDisable(currentPageNumber == lastPage);
    }

    /**
     * Loads controller for handling Books. (Book entities not BooksInOrder!)
     */
    @FXML
    private void startBookList() throws IOException {
        Stage primaryStage = (Stage) listView.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/bookListView.fxml"));
        Parent root = (Parent) fxmlLoader.load();

        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    /**
     * Deletes a book from order.
     */
    @FXML
    private void deleteBookFromOrder(){
        try {
            BookInOrderDao bookInOrderDao = new BookInOrderDao(em);
            em.getTransaction().begin();
            bookInOrderDao.delete(selectedBookInOrder);
            em.getTransaction().commit();
            selectedOrder.getBookInOrder().remove(selectedBookInOrder);
            showDetailOfOrder();
        }

        catch (Exception e){
            if (em.getTransaction().isActive()) em.getTransaction().rollback();
            Alert alert = new Alert(Alert.AlertType.ERROR, "Something went wrong, the book could not be deleted.", ButtonType.OK);
            alert.showAndWait();
        }
    }

    /**
     * Loads controller for adding a book to order.
     */
    @FXML
    private Stage loadAddBookInOrderController() throws IOException {
        Stage bookStage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/addBookInOrder.fxml"));
        Parent root = (Parent) fxmlLoader.load();

        bookStage.setResizable(false);
        bookStage.initModality(Modality.APPLICATION_MODAL);
        bookStage.setTitle("Add new book to order");
        bookStage.setScene(new Scene(root));
        bookStage.show();

        bookStage.setOnHiding(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                em.refresh(selectedOrder);
                showDetailOfOrder();
            }
        });
        return bookStage;
    }

    public static Order getSelectedOrder() {
        return selectedOrder;
    }
}
