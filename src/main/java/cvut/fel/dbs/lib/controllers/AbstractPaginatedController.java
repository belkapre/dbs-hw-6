package cvut.fel.dbs.lib.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;

/**
 * All controllers with pagination extend this controller.
 */
abstract public class AbstractPaginatedController {
    @FXML
    protected Button nextPage;

    @FXML
    protected Button previousPage;

    protected int lastPage;
    protected int currentPageNumber = 1;

    /**
     * Fetches the current page of entries from model and shows it in the list to the user.
     */
    abstract protected void showPage();

    /**
     * Sets the total amount of pages (or the number of the last page)
     */
    abstract protected void setLastPage();

    /**
     * Invoked after clicking the button with right arrow.
     * It shows the next page and tracks, the current page and state of buttons.
     */
    @FXML
    protected void nextPage(){
        currentPageNumber += 1;
        if (currentPageNumber == lastPage){
            nextPage.setDisable(true);
        }
        previousPage.setDisable(false);
        showPage();
    }

    /**
     * Invoked after clicking the button with left arrow.
     * It shows the previous page and tracks, the current page and state of buttons.
     */
    @FXML
    protected void previousPage(){
        currentPageNumber -= 1;
        nextPage.setDisable(false);
        if (currentPageNumber == 1) previousPage.setDisable(true);
        showPage();
    }
}
