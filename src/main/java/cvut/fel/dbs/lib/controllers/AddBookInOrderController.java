package cvut.fel.dbs.lib.controllers;

import cvut.fel.dbs.lib.Application;
import cvut.fel.dbs.lib.dao.BookDao;
import cvut.fel.dbs.lib.dao.BookInOrderDao;
import cvut.fel.dbs.lib.model.Book;
import cvut.fel.dbs.lib.model.BookInOrder;
import cvut.fel.dbs.lib.model.Order;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import java.net.URL;
import java.util.ResourceBundle;

public class AddBookInOrderController implements Initializable {
    @FXML
    Order order;

    @FXML
    private TextField orderId;

    @FXML
    private TextField isbn;

    @FXML
    private TextField quantity;

    /**
     * Invoked after loading the controler with FXMLloader.
     *
     * @param location supplied automatically by JAVAfx
     * @param resources supplied automatically by JAVAfx
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        order = OrderListController.getSelectedOrder();
        orderId.setText(String.valueOf(order.getId()));
    }

    /**
     * Tries to create a new BookInOrder object, that maps the relationship between Book and Order.
     * If the values supplied by the user are invalid, then error dialog is shown.
     */
    @FXML
    private void addBookToOrder() {
        EntityManager em = Application.getEm();
        try {
            BookInOrder bookInOrder = new BookInOrder();
            bookInOrder.setOrder(order);
            bookInOrder.setQuantity(Integer.parseInt(quantity.getText()));
            bookInOrder.setBookByIsbn(isbn.getText());
            BookInOrderDao bookInOrderDao = new BookInOrderDao(Application.getEm());

            em.getTransaction().begin();
            bookInOrderDao.create(bookInOrder);
            em.getTransaction().commit();

            Stage stage = (Stage)isbn.getScene().getWindow();
            stage.close();
        }
        catch (Exception e){
            if (em.getTransaction().isActive()) em.getTransaction().rollback();
            Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage(), ButtonType.OK);
            alert.showAndWait();
        }
    }
}
