package cvut.fel.dbs.lib.controllers;

import cvut.fel.dbs.lib.Application;
import cvut.fel.dbs.lib.dao.BookDao;
import cvut.fel.dbs.lib.model.Book;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.persistence.EntityManager;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class BookListController extends AbstractPaginatedController implements Initializable{
    private boolean entityShownBook = true;
    private static EntityManager em;

    @FXML
    private ListView listView;

    @FXML
    private Button addBookButton;

    private List<Book> currentPage;
    private static Book selectedBook;

    /**
     * Invoked after loading the controler with FXMLloader.
     * Sets up necessary atributtes, listeners and shows the first page.
     *
     * @param location supplied automatically by JAVAfx
     * @param resources supplied automatically by JAVAfx
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.em = Application.getEm();
        showPage();
        addListeners();
        setLastPage();
        previousPage.setDisable(true);
        nextPage.setDisable(lastPage == 0);
    }

    /**
     * Sets the total amount of pages (or the number of the last page)
     */
    @Override
    protected void setLastPage(){
        BookDao bookDao = new BookDao(em);
        lastPage = bookDao.getPages();
    }

    /**
     * Shows info book information, if a user is trying to update an existing book.
     */
    @Override
    protected void showPage(){
        BookDao bookDao = new BookDao(em);
        currentPage = bookDao.findPage(currentPageNumber);
        listView.getItems().clear();
        for (Book book : currentPage){
            listView.getItems().add(book);
        }
    }

    /**
     * Adds listeners for list items and add book button.
     * Called during initialization.
     */
    private void addListeners(){
        listView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                showDetail();
            }
        });

        addBookButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                newBook();
            }
        });
    }

    /**
     * Adds listener, that refreshes the list, when a new book is added or updated.
     *
     * @param bookDetailStage window with details of a book
     */
    private void addListUpdateListener(Stage bookDetailStage){
        bookDetailStage.setOnHiding(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                showPage();
            }
        });
    }

    /**
     * Unselects current book and opens the window for adding a new book.
     */
    private void newBook(){
        selectedBook = null;

        try {
            Stage bookDetailStage = loadBookDetailController();
            addListUpdateListener(bookDetailStage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Opens a window showing details of a selected book.
     */
    private void showDetail(){
        if (listView.getSelectionModel().getSelectedItem() == null) return;
        selectedBook = (Book) listView.getSelectionModel().getSelectedItem();

        try {
            Stage bookDetailStage = loadBookDetailController();
            addListUpdateListener(bookDetailStage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Loads controller for handling the book detail window.
     *
     * @return Stage of the window
     */
    private Stage loadBookDetailController() throws IOException {
        Stage bookStage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/bookDetailView.fxml"));
        Parent root = (Parent) fxmlLoader.load();

        bookStage.setResizable(false);
        bookStage.initModality(Modality.APPLICATION_MODAL);
        bookStage.setTitle(selectedBook != null ? selectedBook.getName() : "Add new book");
        bookStage.setScene(new Scene(root));
        bookStage.show();
        return bookStage;
    }

    public static Book getSelectedBook() {
        return selectedBook;
    }

    public static EntityManager getEm() {
        return em;
    }

    /**
     * Called after choosing order entity in the top navbar.
     * Stars controller for showing orders.
     */
    @FXML
    private void startOderList() throws IOException {
        Stage primaryStage = (Stage) listView.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/orderListView.fxml"));
        Parent root = (Parent) fxmlLoader.load();

        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
