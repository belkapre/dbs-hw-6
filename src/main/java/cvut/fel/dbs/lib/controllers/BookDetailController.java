package cvut.fel.dbs.lib.controllers;

import cvut.fel.dbs.lib.dao.BookDao;
import cvut.fel.dbs.lib.model.Book;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import java.net.URL;
import java.util.ResourceBundle;

public class BookDetailController implements Initializable{
    private Book book;

    private EntityManager em;

    private boolean update;

    @FXML
    private TextField isbn;

    @FXML
    private TextField name;

    @FXML
    private TextField author;

    @FXML
    private TextField edition;

    @FXML
    private TextField publisher;

    @FXML
    private TextField releaseYear;

    @FXML
    private TextField price;

    @FXML
    private Button delete;

    @FXML
    private Button save;

    private Stage stage;

    /**
     * Invoked after loading the controler with FXMLloader.
     * It decides whether the user is trying to update info about a book or insert a new book.
     *
     * @param location supplied automatically by JAVAfx
     * @param resources supplied automatically by JAVAfx
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        book = BookListController.getSelectedBook();
        if (book != null) showBook();
        else{
            update = false;
            book = new Book();
            changeButtonsForNewBook();
        }
        this.em = BookListController.getEm();
    }

    /**
     * Shows info book information, if a user is trying to update an existing book.
     */
    private void showBook(){
        if (book.getIsbn() != null) isbn.setDisable(true);
        isbn.setText(book.getIsbn());
        name.setText(book.getName());
        edition.setText(String.valueOf(book.getEdition()));
        publisher.setText(book.getPublisher());
        releaseYear.setText(String.valueOf(book.getReleaseyear()));
        author.setText(book.getAuthor());
        price.setText(String.valueOf(book.getPrice()));
    }

    /**
     * Show different buttons if user is creating a new book.
     */
    private void changeButtonsForNewBook(){
        delete.setDisable(true);
        save.setText("Add");
    }

    /**
     * Tries to save the information provided by user.
     * The entity is either merged or persisted depending on,
     * whether the user is creating a new book or updating an exisintg one.
     *
     */
    @FXML
    private void createOrUpdate(){
        try {
            book.setReleaseyear(Integer.parseInt(releaseYear.getText()));
            book.setName(name.getText());
            book.setPrice(Integer.parseInt(price.getText()));
            book.setAuthor(author.getText());
            book.setEdition(Integer.parseInt(edition.getText()));
            book.setPublisher(publisher.getText());
            if (update) update();
            else create();
            Stage stage = (Stage)author.getScene().getWindow();
            stage.close();
        }
        catch(Exception e){
            if (em.getTransaction().isActive()) em.getTransaction().rollback();
            Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage(), ButtonType.OK);
            alert.showAndWait();
        }
    }

    /**
     * Updates an existing book.
     */
    private void update(){
        BookDao bookDao = new BookDao(em);
        em.getTransaction().begin();
        bookDao.merge(book);
        em.getTransaction().commit();
    }

    /**
     * Persists a new book.
     */
    private void create(){
        book.setIsbn(isbn.getText());
        BookDao bookDao = new BookDao(em);
        em.getTransaction().begin();
        bookDao.create(book);
        em.getTransaction().commit();
    }

    /**
     * Deletes a book.
     */
    @FXML
    private void delete(){
        try {
            String isbn = this.isbn.getText();
            BookDao bookDao = new BookDao(em);
            em.getTransaction().begin();
            Book book = bookDao.find(isbn);
            bookDao.delete(book);
            em.getTransaction().commit();
            Stage stage = (Stage)author.getScene().getWindow();
            stage.close();
        }

        catch (Exception e){
            if (em.getTransaction().isActive()) em.getTransaction().rollback();
            Alert alert = new Alert(Alert.AlertType.ERROR, "Something went wrong, the book could not be deleted.", ButtonType.OK);
            alert.showAndWait();
        }
    }
}
