package cvut.fel.dbs.lib;

import cvut.fel.dbs.lib.dao.OrderDao;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Application extends javafx.application.Application {

    private static EntityManagerFactory emf;
    private static EntityManager em;

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/bookListView.fxml"));
        Parent root = (Parent) fxmlLoader.load();

        primaryStage.setTitle("DBS-HW6");
        primaryStage.setScene(new Scene(root));

        primaryStage.show();

        primaryStage.setOnHiding(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                close();
            }
        });
    }

    public static void setup(){
        emf = Persistence.createEntityManagerFactory("pu");
        em = emf.createEntityManager();;
    }

    public void close(){
        em.close();
        emf.close();
    }

    public static void main(String[] args) {
        setup();
        launch(args);
    }

    public static EntityManager getEm() {
        return em;
    }
}
