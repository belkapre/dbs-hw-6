package cvut.fel.dbs.lib.seeder;

import com.opencsv.CSVReader;
import cvut.fel.dbs.lib.dao.BookDao;
import cvut.fel.dbs.lib.model.Book;

import javax.persistence.EntityManager;
import java.io.FileReader;
import java.util.Random;

/**
 * Was only used once to seed the data into the DB.
 */
public class BookSeeder {
    public static String[] vats = {"12345678A", "22344678X", "12345624C", "12345178F", "12315678D"};

    /**
     * Seeds sample books into the DB
     */
    public static void seedBooks(EntityManager em, int numberOfBooks){
        String row[];
        int counter = 0;
        BookDao bookDao = new BookDao(em);
        try {
        CSVReader csvReader = new CSVReader(new FileReader("src/main/resources/sampleData/book.csv"));
            while ((row = csvReader.readNext()) != null && counter != numberOfBooks) {
                if (counter == 0) {
                    counter++;
                    continue;
                }
                Book book = getBook(row, em);
                em.getTransaction().begin();
                bookDao.create(book);
                em.getTransaction().commit();
                counter++;
            }
        }
        catch (Exception e){
            System.out.println(e);
        }
    }

    private static Book getBook(String[] data, EntityManager em){
        Random rng = new Random();
        Book book = new Book();
        book.setName(data[1]);
        book.setAuthor(data[2]);
        book.setIsbn(data[8]);
        book.setPrice(rng.nextInt(450) + 400);
        book.setEdition(rng.nextInt(4) + 1);
        book.setReleaseyear(Integer.parseInt(data[13].split(" ")[1]));
        book.setPublisher(vats[rng.nextInt(4)]);

        return book;
    }
}
