package cvut.fel.dbs.lib.seeder;

import com.opencsv.CSVReader;
import cvut.fel.dbs.lib.dao.BookDao;
import cvut.fel.dbs.lib.dao.OrderDao;
import cvut.fel.dbs.lib.model.*;

import javax.persistence.EntityManager;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Was only used once to seed data into the DB.
 */
public class OrderSeeder {

    private static ArrayList<String> isbns = new ArrayList<>();

    public static void seedOrders(EntityManager em, int numberOfOrders){
        loadIsbns();
        Random rng = new Random();
        BookDao bookDao = new BookDao(em);
       // CustomerDao customerDao = new CustomerDao(em);
        //customers = customerDao.findAll();
        OrderDao orderDao = new OrderDao(em);

        for (int i = 0; i < numberOfOrders; i++){
            Order order = createOrder(bookDao, i);
            em.getTransaction().begin();
            orderDao.create(order);
            em.getTransaction().commit();
        }
    }

    private static Order createOrder(BookDao bookDao, int id){
        Random rng = new Random();
        Order order = new Order();
        String deliveryTrackingNumber = generateRandomDeliveryTrackingNumber();
        //Customer customer = getRandomCustomer();
        ArrayList<BookInOrder> books = new ArrayList<BookInOrder>();
        Integer price = 0;

        int numberOfBooks = rng.nextInt(4) + 1;

        for (int i = 0; i < numberOfBooks; i++){
            Book book = getRandomBook(rng, bookDao);
            price += book.getPrice();
            BookInOrder bookInOrder = new BookInOrder();
            bookInOrder.setQuantity(rng.nextInt(3) + 1);
            bookInOrder.setBook(book);
            bookInOrder.setOrder(order);
            books.add(bookInOrder);
        }

        order.setPrice(price);
        //order.setCustomer(customer);
        order.setBookInOrder(books);
        order.setOrderPlacedTimestamp(new Date());
        order.setDeliveryTrackingNumber(deliveryTrackingNumber);
        order.setId(id);

        return order;
    }

    private static Book getRandomBook(Random rng, BookDao bookDao){
        int randomIndex = rng.nextInt(49);
        return bookDao.find(isbns.get(randomIndex));
    }

    /**private static Customer getRandomCustomer(){
        Random rng = new Random();
        int randomIndex = rng.nextInt(49);
        return customers.get(randomIndex);
    }*/

    private static void loadIsbns(){
        String row[];
        int counter = 0;
        try {
            CSVReader csvReader = new CSVReader(new FileReader("src/main/resources/sampleData/book.csv"));
            while ((row = csvReader.readNext()) != null && counter != 50) {
                if (counter == 0) {
                    counter++;
                    continue;
                }
                addIsbn(row);
                counter++;
            }
        }
        catch (Exception e){
            System.out.println(e);
        }
    }

    private static void addIsbn(String[] row){
        isbns.add(row[8]);
    }

    private static String generateRandomDeliveryTrackingNumber(){
        char[] characters = {'1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd'};
        String result = "";
        for (int i = 0; i < 25; i++){
            Random rng = new Random();
            result += characters[rng.nextInt(12)];
        }
        return result;
    }
}
